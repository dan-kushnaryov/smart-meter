/* eslint-disable no-console */
const fs = require('fs');
const _ = require('lodash');
const xml2json = require('xml2json');

const app = require('../src/app');
const sequelizeClient = app.get('sequelizeClient');
const logger = app.get('logger');
const xmlDataSet = fs.readFileSync(__dirname + '/../data/dataset.xml');
const { response: dataSet } = xml2json.toJson(xmlDataSet, { object: true });

logger.info('xml dataset successfully converted to json');

const { energy_statistic: energyStatisticModel  } = sequelizeClient.models;
const data = dataSet.row.row;
const CHUNK_SIZE = 100;

let countData = data.length;

energyStatisticModel.sync({ force: true }).then(() => {
  storeByChunks({ data });
});

process.on('unhandledRejection', reason =>
  logger.error({
    type: 'error',
    message: reason.message
  })
);

async function storeByChunks({ data }) {
  const chunkedData = _.chunk(data, CHUNK_SIZE);

  await energyStatisticModel.destroy({ where: {}, truncate: true });

  for (const schoolsList of chunkedData) {
    await storeSchoolList(schoolsList);
  }

  await sequelizeClient.close();
}

async function storeSchoolList(schoolsList) {
  await Promise.all(
    _.map(schoolsList, async school => {
      _.forEach(getListOfTimeRange(), timeRange => {
        const kwH30m = _.get(school, timeRange);
        const kwM30m = _.round(kwH30m / 60, 2);

        _.set(school, timeRange, kwM30m);
      });

      const storedSchool = await energyStatisticModel.create(school);
      
      countData -= 1;
      
      logger.info({
        type: 'info',
        message: `The school stored with id: ${storedSchool.get('id')}`
      });
    })
  );

  logger.info({
    type: 'info',
    message: `The process has been paused. ${countData} rows left.`
  });
}

function getListOfTimeRange() {
  return [
    '_00_00',
    '_00_30',
    '_01_00',
    '_01_30',
    '_02_00',
    '_02_30',
    '_03_00',
    '_03_30',
    '_04_00',
    '_04_30',
    '_05_00',
    '_05_30',
    '_06_00',
    '_06_30',
    '_07_00',
    '_07_30',
    '_08_00',
    '_08_30',
    '_09_00',
    '_09_30',
    '_10_00',
    '_10_30',
    '_11_00',
    '_11_30',
    '_12_00',
    '_12_30',
    '_13_00',
    '_13_30',
    '_14_00',
    '_14_30',
    '_15_00',
    '_15_30',
    '_16_00',
    '_16_30',
    '_17_00',
    '_17_30',
    '_18_00',
    '_18_30',
    '_19_00',
    '_19_30',
    '_20_00',
    '_20_30',
    '_21_00',
    '_21_30',
    '_22_00',
    '_22_30',
    '_23_00',
    '_23_30'
  ];
}