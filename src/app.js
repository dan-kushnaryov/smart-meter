const config = require('../config');
const path = require('path');
const favicon = require('serve-favicon');
const compress = require('compression');
const cors = require('cors');
const helmet = require('helmet');
const logger = require('./logger');

const errorHandler = require('@feathersjs/errors/lib/error-handler');
const feathers = require('@feathersjs/feathers');
const configuration = require('@feathersjs/configuration');
const express = require('@feathersjs/express');
const socketio = require('@feathersjs/socketio');

const middleware = require('./middleware');
const services = require('./services');
const appHooks = require('./app.hooks');
const channels = require('./channels');

const sequelize = require('./sequelize');

const app = express(feathers());

app.set('logger', logger);
app.set('config', config);

// Load app configuration
app.configure(configuration());
// Enable CORS, security, compression, favicon and body parsing
app.use(cors());
app.use(helmet());
app.use(compress());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(favicon(path.join(app.get('public'), 'favicon.ico')));

// Host the public folder
app.use('/', express.static(app.get('public')));

// Set up Plugins and providers
app.configure(socketio());
app.configure(express.rest());
app.configure(sequelize);

// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
// Set up our services (see `services/index.js`)
app.configure(services);
// Set up event channels (see channels.js)
app.configure(channels);

// Configure a middleware for 404s and the error handler
app.use(express.notFound());
app.use(
  errorHandler({
    json: {
      400: (error, request, response) => {
        response.status(error.code).json(error.errors);
      },
      404: (error, request, response) => {
        response.status(error.code).json();
      },
      500: (error, request, response) => {
        const message = error.message ? [error.message] : [];

        response.status(error.code).json(message);
      },
      default: (error, request, response) => {
        const responseError = {
          code: error.code || 500,
          errors: error.message ? [error.message] : []
        };

        response.status(responseError.code).json(responseError);
      }
    }
  })
);

app.hooks(appHooks);

module.exports = app;
