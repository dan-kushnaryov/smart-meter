'use strict';

const bunyan = require('bunyan');
const fs = require('fs');
const path = require('path');
const rootPath = require('app-root-path');
const reqlib = rootPath.require;
const config = reqlib('/config').bunyan;

function create() {
  if (!config.logDir)
    throw new Error('The logDir is required for the logger config.');
  if (!config.logLevel)
    throw new Error('The logLevel is required for the logger config.');
  if (!config.logFileName)
    throw new Error('The logFileName is required for the logger config.');

  try {
    fs.statSync(config.logDir);
  } catch (e) {
    fs.mkdirSync(config.logDir);
  }

  const logLevels = ['debug', 'info', 'warn', 'error', 'fatal'];
  const loggers = {};

  logLevels.forEach(level => {
    const stream = { level };

    if (level !== 'debug') {
      stream['path'] = path.join(
        config.logDir,
        `${config.logFileName}-${level}.log`
      );
    } else {
      stream['stream'] = process.stdout;
    }

    const logger = bunyan.createLogger({
      name: config.logFileName.replace('.log', ''),
      serializers: { err: bunyan.stdSerializers.err },
      streams: [stream]
    });

    loggers[level] = data => {
      if (process.env.NODE_ENV === 'test') {
        return;
      }
      logger[level](data);
      if (config.logDebug === true && level !== 'debug') {
        loggers.debug(data);
      }
    };
  });

  return loggers;
}

module.exports = create();