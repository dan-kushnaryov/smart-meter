// eslint-disable-next-line no-unused-vars
const energyStatisticModelCreate = require('../models/energy-statistic.model');

module.exports = function (app) {
  // Add your custom middleware here. Remember, that
  // in Express the order matters
  energyStatisticModelCreate(app);
};
