/* eslint-disable no-unused-vars */
class Service {
  constructor ({ energyStatisticModel }) {
    this.energyStatisticModel = energyStatisticModel;
  }

  async find (params, data) {
    const schools = await this.energyStatisticModel.findAll({
      where: {
        school: params.query.school,
        type: params.query.type
      },
      attributes: this.attributes,
      order: [['date', 'DESC']]
    });

    return {
      school: params.query.school,
      type: params.query.type,
      data: schools
    };
  }

  create (data) {
    return Promise.resolve(data);
  }

  get attributes() {
    return ['school', 'type', ['date', 'DATA_DATE'], this.currentTimeRange];
  }

  get currentTimeRange() {
    const currentTime = new Date();
    const timeRange = `_${currentTime.getHours()}${currentTime.getMinutes() < 30 ? '_00' : '_30'}`; 

    return [timeRange, 'VALUE_NUMERIC'];
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
