

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [function(hook) {
      hook.result = [
        {
          data: hook.result.data,
          metric: {
            'METRIC_ID': 1,
            'INDICATOR_ID': 1,
            'NAME': `Energy type: ${hook.result.type}`,
            'DESCRIPTION': 'Sample School Energy Data',
            'Y_AXIS_NAME': 'kWm/30 mins',
            'UNIT_ID': 1,
            'TIMING_ID': null,
            'SORT_ORDER': 2,
            'AUTOMATED': 'Y',
            'ASSIGNED_USER': null,
            'META_NUMERIC_1_TITLE': null,
            'META_VARCHAR_1_TITLE': null,
            'GOAL_NUMERIC': null,
            'TARGET_OTHER': null,
            'TARGET_OTHER_NAME': null,
            'TARGET_OTHER_COLOR': null,
            'CHART_TYPE': null,
            'CHART_SCALE': null,
            'CHART_INTERVAL': null,
            'QUERY': null,
            'QUERY_FIELDS': null,
            'METRIC_SOURCE': null,
            'WIDGET_SETTINGS': '{\r\n    "detail":{\r\n        "type":"bar"\r\n    }\r\n}',
            'IND_NAME': 'Early Alert',
            'AREA_NAME': `School: ${hook.result.school}`
          }
        }
      ];

      return hook;
    }],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
