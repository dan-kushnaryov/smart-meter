// Initializes the `emitter` service on path `/emitter`
const createService = require('./emitter.class.js');
const hooks = require('./emitter.hooks');

module.exports = function (app) {
  const { models } = app.set('sequelizeClient');

  const options = {
    name: 'emitter',
    energyStatisticModel: models.energy_statistic
  };

  // Initialize our service with any options it requires
  app.use('/emitter', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('emitter');

  service.hooks(hooks);
};
