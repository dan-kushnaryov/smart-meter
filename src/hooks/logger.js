const logger = require('../logger');

module.exports = function() {
  return function(hook) {
    if (process.env.NODE_ENV !== 'test') {
      let message = `${hook.type}: ${hook.path} - Method: ${hook.method}`;

      if (hook.type === 'error') {
        message += `: ${hook.error.message}`;
      }

      logger.info({ type: 'info', message, meta: {} });
      logger.debug({ type: 'debug', message: 'hook.data', meta: hook.data });
      logger.debug({
        type: 'debug',
        message: 'hook.params',
        meta: hook.params
      });

      if (hook.result) {
        logger.debug({
          type: 'debug',
          message: 'hook.result',
          meta: hook.result
        });
      }

      if (hook.error) {
        const { error, data } = hook;

        logger.error({
          type: 'error',
          errorName: error.name,
          message: error.errors,
          meta: {
            statusCode: error.code,
            stack: error.stack,
            path: hook.path,
            data
          }
        });
      }
    }
  };
};