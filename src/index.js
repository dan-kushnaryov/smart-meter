/* eslint-disable no-console */
const app = require('./app');
const logger = app.get('logger');
const port = app.get('port');
const server = app.listen(port);

setInterval(() => {
  app.service('emitter').create({});
}, 5000);

process.on('unhandledRejection', (reason, p) =>
  logger.error({
    type: 'error',
    message: `Unhandled Rejection at: Promise - ${reason.message}`,
    meta: { p, reason }
  })
);

server.on('listening', () =>
  logger.info('Feathers application started on http://%s:%d', app.get('host'), port)
);
