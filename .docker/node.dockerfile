FROM node:carbon

ENV NODE_ENV=development 
ENV PORT=3030

RUN mkdir -p /var/www
WORKDIR   /var/www
COPY package.json ./

RUN npm install
RUN npm install --global nodemon

COPY . /var/www

RUN ["chmod", "+x", ".docker/postgres_scripts/populate.sh"]