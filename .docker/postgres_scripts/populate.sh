#!/bin/bash

set -e

host="$1"
shift
cmd="$@"

sleep 5

>&2 echo "Postgres is up - executing command"

DOCKER_POSTGRES_HOST=postgres_smart_meter node /var/www/console/dataset-parser.js & wait

exec $cmd