#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE USER smartmeter WITH password 'smartmeter';
    CREATE DATABASE smartmeter;
    CREATE DATABASE smartmeter_test;
    GRANT ALL ON DATABASE smartmeter TO smartmeter;
    GRANT ALL ON DATABASE smartmeter_test TO smartmeter;
    ALTER DATABASE smartmeter OWNER TO smartmeter;
    ALTER DATABASE smartmeter_test OWNER TO smartmeter;
    \c smartmeter;
    create extension "uuid-ossp";
    \c smartmeter_test;
    create extension "uuid-ossp";
EOSQL