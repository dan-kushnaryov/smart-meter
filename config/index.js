const config = require('./default.json');

if ('DOCKER_POSTGRES_HOST' in process.env) {
  config.db.host = process.env.DOCKER_POSTGRES_HOST
}

module.exports = config;