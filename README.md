# Smart Meter

## How to use:

For running service with Docker, you should follow this steps:

1. Make sure you have [Docker Community Edition](https://www.docker.com/community-edition)

1. Clone this repo.

1. Run `docker -v`, it will return:

   ```shell
   content-docker (master) $ docker -v
   Docker version 17.12.0-ce, build c97c6d6
   ```

   * If return an error, you should run `docker-machine start && eval $(docker-machine env)`

1. Run `docker-compose build`

1. Run `docker-compose up`

1. Goto to browser and go to [localhost:3030](http://localhost:3030/)