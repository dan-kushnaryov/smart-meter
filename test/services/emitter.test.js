const assert = require('assert');
const app = require('../../src/app');

describe('\'emitter\' service', () => {
  it('registered the service', () => {
    const service = app.service('emitter');

    assert.ok(service, 'Registered the service');
  });
});
